package test.com.stars.service;

import jdk.jfr.MemoryAddress;
import main.com.stars.services.StarsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class StarsServiceTest {

    StarsService cut = new StarsService();

   @Test
    void fillSquareTest(){
        String expected = " 11 12 13 14 15 16 17\n" +
                " 21 22 23 24 25 26 27\n" +
                " 31 32 33 34 35 36 37\n" +
                " 41 42 43 44 45 46 47\n" +
                " 51 52 53 54 55 56 57\n" +
                " 61 62 63 64 65 66 67\n" +
                " 71 72 73 74 75 76 77\n";
        String actual = cut.fillSquare();
       Assertions.assertEquals(expected, actual);
    }
    @Test
    void fillEmptySquare(){
        String expected =  " 11 12 13 14 15 16 17\n" +
                            " 21                27\n" +
                            " 31                37\n" +
                            " 41                47\n" +
                            " 51                57\n" +
                            " 61                67\n" +
                            " 71 72 73 74 75 76 77\n";
        String actual = cut.fillEmptySquare();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    void getUpLeftTriangle(){
        String expected = " 11 12 13 14 15 16 17\n" +
                            " 21             26   \n" +
                            " 31          35      \n" +
                            " 41       44         \n" +
                            " 51    53            \n" +
                            " 61 62               \n" +
                            " 71                  \n" ;
        String actual = cut.getUpLeftTriangle();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    void getDownLeftTriangle(){
        String expected = " 11                  \n" +
                        " 21 22               \n" +
                        " 31    33            \n" +
                        " 41       44         \n" +
                        " 51          55      \n" +
                        " 61             66   \n" +
                        " 71 72 73 74 75 76 77\n";
        String actual = cut.getDownLeftTriangle();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    void getDownRightTriangle(){
        String expected =   "                   17\n" +
                            "                26 27\n" +
                            "             35    37\n" +
                            "          44       47\n" +
                            "       53          57\n" +
                            "    62             67\n" +
                            " 71 72 73 74 75 76 77\n";
        String actual = cut.getDownRightTriangle();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    void getUpRightTriangle(){
        String expected = " 11 12 13 14 15 16 17\n" +
                        "    22             27\n" +
                        "       33          37\n" +
                        "          44       47\n" +
                        "             55    57\n" +
                        "                66 67\n" +
                        "                   77\n";
        String actual = cut.getUpRightTriangle();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    void getCross(){
        String expected =   " 11                17\n" +
                            "    22          26   \n" +
                            "       33    35      \n" +
                            "          44         \n" +
                            "       53    55      \n" +
                            "    62          66   \n" +
                            " 71                77\n";
        String actual = cut.getCross();
        Assertions.assertEquals(expected, actual);

    }
    @Test
    void getTriangleUp(){
        String expected =" 11 12 13 14 15 16 17\n" +
                        "    22          26   \n" +
                        "       33    35      \n" +
                        "          44         \n" +
                        "                     \n" +
                        "                     \n" +
                        "                     \n" ;
        String actual = cut.getTriangleUp();
        Assertions.assertEquals(expected, actual);

    }
    @Test
    void getTriangleDown(){
        String expected =   "                     \n" +
                            "                     \n" +
                            "                     \n" +
                            "          44         \n" +
                            "       53    55      \n" +
                            "    62          66   \n" +
                            " 71 72 73 74 75 76 77\n";
        String actual = cut.getTriangleDown();
        Assertions.assertEquals(expected, actual);

    }

}
