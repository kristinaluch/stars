package main.com.stars;

import main.com.stars.services.StarsService;

public class Main {
    public static void main(String[] args) {
        StarsService starsService = new StarsService();
        System.out.println(starsService.fillSquare());
        System.out.println(starsService.fillEmptySquare());
        System.out.println(starsService.getUpLeftTriangle());
        System.out.println(starsService.getDownLeftTriangle());
        System.out.println(starsService.getDownRightTriangle());
        System.out.println(starsService.getUpRightTriangle());
        System.out.println(starsService.getCross());
        System.out.println(starsService.getTriangleUp());
        System.out.println(starsService.getTriangleDown());
    }
}
