package main.com.stars.services;

public class StarsService {
    private static final int LINE_VERT = 10;
    private int[][] array = {
                            {11,12,13,14,15,16,17},
                            {21,22,23,24,25,26,27},
                            {31,32,33,34,35,36,37},
                            {41,42,43,44,45,46,47},
                            {51,52,53,54,55,56,57},
                            {61,62,63,64,65,66,67},
                            {71,72,73,74,75,76,77}
                            };


    public String fillSquare(){
        String result = "";
        for (int line = 0;  line<array.length; line++) {
            for (int number = 0; number < array[line].length; number++) {
                result = (result +" "+array[line][number]);
            }
            result = result +"\n";
        }
        return result;
    }

    public String fillEmptySquare(){
        String result = "";
        for (int line = 0;  line<array.length; line++) {
            for (int number = 0; number < array[line].length; number++) {
                if(line>0&&line<6&&number>0&&number<6){
                    result = result+"   ";
                } else {
                    result = (result + " " + array[line][number]);
                }
            }
            result = result +"\n";
        }
        return result;
    }


    public String getUpLeftTriangle(){
        String result = "";
        boolean printNumber;
        for (int line = 0;  line<array.length; line++) {

            for (int number = 0; number < array[line].length; number++) {
                printNumber = line==0||number == 0||number==(6-line);
                if(printNumber){
                    result = (result + " " + array[line][number]);
                } else {
                    result = result+"   ";
                }
            }
            result = result +"\n";
        }
        return result;
    }

   public String getDownLeftTriangle(){
        String result = "";
            boolean printNumber;
                for (int line = 0;  line<array.length; line++) {

                for (int number = 0; number < array[line].length; number++) {
                    printNumber = line==(array.length-1)||number == 0||number==line;
                    if(printNumber){

                        result = (result + " " + array[line][number]);
                    } else {
                        result = result+"   ";
                    }
                }
                result = result +"\n";
            }
                return result;
    }


   public String getDownRightTriangle(){
        String result = "";
        boolean printNumber;
        int last = array.length - 1;
        for (int line = 0;  line<array.length; line++) {

            for (int number = 0; number < array[line].length; number++) {

                printNumber = line==last||number == last||number==(last-line);

                if(printNumber){

                    result = (result + " " + array[line][number]);
                } else {
                    result = result+"   ";
                }
            }
            result = result +"\n";
        }
        return result;
   }

    public String getUpRightTriangle(){
        String result = "";
        boolean printNumber;
        int last = array.length - 1;
        for (int line = 0;  line<array.length; line++) {

            for (int number = 0; number < array[line].length; number++) {

                printNumber = line==0||number == last||number==line;

                if(printNumber){

                    result = (result + " " + array[line][number]);
                } else {
                    result = result+"   ";
                }
            }
            result = result +"\n";
        }
        return result;
    }

    public String getCross(){
        String result = "";
        boolean printNumber;
        int last = array.length - 1;
        for (int line = 0;  line<array.length; line++) {

            for (int number = 0; number < array[line].length; number++) {

                printNumber = number==line||number == (last-line);

                if(printNumber){

                    result = (result + " " + array[line][number]);
                } else {
                    result = result+"   ";
                }
            }
            result = result +"\n";
        }
        return result;
    }

    public String getTriangleUp(){
        String result = "";
        boolean printNumber;
        int last = array.length - 1;
        for (int line = 0;  line<array.length; line++) {

            for (int number = 0; number < array[line].length; number++) {

                printNumber = (number==line)&&(number<=(last/2))||((number == (last-line))&&(number>=(last/2)))||line == 0;

                if(printNumber){

                    result = (result + " " + array[line][number]);
                } else {
                    result = result+"   ";
                }
            }
            result = result +"\n";
        }
        return result;
    }

    public String getTriangleDown(){
        String result = "";
        boolean printNumber;
        int last = array.length - 1;
        for (int line = 0;  line<array.length; line++) {

            for (int number = 0; number < array[line].length; number++) {

                printNumber = (number==line)&&(number>=(last/2))||((number == (last-line))&&(number<=(last/2)))||line == last;

                if(printNumber){

                    result = (result + " " + array[line][number]);
                } else {
                    result = result+"   ";
                }
            }
            result = result +"\n";
        }
        return result;
    }

}
